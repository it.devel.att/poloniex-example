package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

const (
	orderType = "o"
	tradeType = "t"
)

type (
	Metric interface {
		ErrorInc(labels ...string)
	}
	metric struct{}

	RequestData struct {
		Poloniex []string `json:"poloniex"`
	}

	RecentTrade struct {
		ID        string    // ID транзакции
		Pair      string    // Торговая пара (из списка выше)
		Price     float64   // Цена транзакции
		Amount    float64   // Объём транзакции
		Side      string    // Как биржа засчитала эту сделку (как buy или как sell)
		Timestamp time.Time // Время транзакции
	}

	RequestPayload struct {
		Command string `json:"command"`
		Channel string `json:"channel"`
	}

	PoloniexHandler struct {
		log    *log.Logger
		ctx    context.Context
		cancel context.CancelFunc
		wg     sync.WaitGroup
		appWG  *sync.WaitGroup
		wsConn *websocket.Conn
		stopCh chan struct{}

		metric Metric

		tradeCh         chan RecentTrade
		poloniexChannel string
		originalChannel string
	}
)

func main() {
	url := "wss://api2.poloniex.com"
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	mainWG := &sync.WaitGroup{}
	mainContext, mainCancel := context.WithCancel(context.Background())

	rd := &RequestData{}
	requestData := []byte(`{"poloniex":["BTC_USDT", "TRX_USDT", "ETH_USDT"]}`)
	err := json.Unmarshal(requestData, rd)
	if err != nil {
		log.Fatal("Unmarshall err: ", err)

	}

	for _, channel := range rd.Poloniex {
		currencies := strings.Split(channel, "_")
		if len(currencies) == 2 {
			poloniexChannel := strings.Join([]string{currencies[1], currencies[0]}, "_")
			handler, err := NewPoloniexHandler(mainContext, mainWG, url, channel, poloniexChannel)
			if err != nil {
				log.Printf("Error while try to create poloniex handler: %v\n", err)
				return
			}
			handler.Run()
		} else {
			log.Fatalf("Wrong currency pair %v\n", channel)
		}
	}

	for {
		select {
		case <-interrupt:
			log.Println("interrupt")
			mainCancel()
			mainWG.Wait()
			return
		}
	}
}

// TODO Stub
func (*metric) ErrorInc(labels ...string) {}

func NewPoloniexHandler(ctx context.Context, wg *sync.WaitGroup, url, channel, poloniexChannel string) (*PoloniexHandler, error) {
	ctx, cancel := context.WithCancel(ctx)
	c, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		cancel()
		return nil, err
	}
	return &PoloniexHandler{
		log:             log.New(os.Stdout, "", 0),
		ctx:             ctx,
		cancel:          cancel,
		appWG:           wg,
		wsConn:          c,
		metric:          &metric{},
		stopCh:          make(chan struct{}),
		tradeCh:         make(chan RecentTrade, 100),
		originalChannel: channel,
		poloniexChannel: poloniexChannel,
	}, nil
}

func (ph *PoloniexHandler) Run() {
	ph.processTrades()
	ph.subscribeToChannel()
	ph.stopHandlerListener()
}

func (ph *PoloniexHandler) Stop() {
	ph.cancel()
	ph.wg.Wait()
	if err := ph.wsConn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, "")); err != nil {
		ph.metric.ErrorInc(ph.poloniexChannel, "err_write_close_message")
		ph.log.Println("write close:", err)
	}
	if err := ph.wsConn.Close(); err != nil {
		ph.metric.ErrorInc(ph.poloniexChannel, "err_close_ws_conn")
		ph.log.Println("write close:", err)
	}
}

func (ph *PoloniexHandler) processTrades() {
	ph.wg.Add(1)
	ph.appWG.Add(1)

	go func() {
		defer ph.wg.Done()
		defer ph.appWG.Done()

		for {
			select {
			case <-ph.ctx.Done():
				ph.log.Printf("[PoloniexHandler.processTrades.%v] Receive done signal", ph.poloniexChannel)
				for trade := range ph.tradeCh {
					if trade.ID != "" {
						ph.handleTrade(trade)
					}
				}
				return
			case trade := <-ph.tradeCh:
				ph.handleTrade(trade)
			}
		}
	}()
}

func (ph *PoloniexHandler) handleTrade(trade RecentTrade) {
	log.Printf("%+v", trade)
}

func (ph *PoloniexHandler) subscribeToChannel() {
	ph.wg.Add(1)
	ph.appWG.Add(1)

	go func() {
		payload := RequestPayload{Command: "subscribe", Channel: ph.poloniexChannel}
		logPrefix := fmt.Sprintf("[PoloniexHandler.subscribeToChannel.%v]", ph.poloniexChannel)
		defer ph.wg.Done()
		defer ph.appWG.Done()
		defer close(ph.tradeCh)

		data, err := json.Marshal(payload)
		if err != nil {
			ph.log.Printf("%v Error while try to write message to ws: %v\n", logPrefix, err)
			ph.metric.ErrorInc(ph.poloniexChannel, "error_unmarshall_payload")
			ph.stopHandler()
			return
		}
		if err := ph.wsConn.WriteMessage(websocket.TextMessage, data); err != nil {
			ph.log.Printf("%v Error while try to write message to ws: %v\n", logPrefix, err)
			ph.metric.ErrorInc(ph.poloniexChannel, "error_write_message_to_ws")
			ph.stopHandler()
			return
		}

		firstMessage := true
		ph.log.Printf("%v Start processing poloniex channel: %v\n", logPrefix, ph.poloniexChannel)
		for {
			select {
			case <-ph.ctx.Done():
				ph.log.Printf("%v Receive done signal\n", logPrefix)
				return
			default:
				_, message, err := ph.wsConn.ReadMessage()
				if err != nil {
					ph.log.Printf("%v Error while try to read message from ws: %v\n", logPrefix, err)
					ph.metric.ErrorInc(ph.poloniexChannel, "error_read_message_from_ws")
					ph.stopHandler()
					return
				}
				if firstMessage {
					// Discard first message
					firstMessage = false
					continue
				}
				ph.processMessage(message)
			}
		}
	}()
}

func (ph *PoloniexHandler) processMessage(message []byte) {
	var data []interface{}
	if err := json.Unmarshal(message, &data); err != nil {
		ph.metric.ErrorInc(ph.poloniexChannel, "error_unmarshall_message")
		ph.log.Printf("[%v] Error: %v\n", ph.poloniexChannel, err)
		return
	}

	if len(data) >= 3 {
		defer func() {
			if r := recover(); r != nil {
				ph.metric.ErrorInc(ph.poloniexChannel, "panic_on_parse")
				ph.log.Printf("[%v] Panic: %v\n", ph.poloniexChannel, r)
			}
		}()
		// data[0] <channel id>
		// data[1] <sequence number>
		rows := data[2].([]interface{})

		for _, row := range rows {
			row := row.([]interface{})
			rowType := row[0].(string)
			switch rowType {
			case orderType:
				// TODO Process order
			case tradeType:
				trade, err := parseTrade(ph.originalChannel, row)
				if err != nil {
					ph.metric.ErrorInc(ph.poloniexChannel, "error_parse_trade", err.Error())
					ph.log.Printf("[%v] Error parse trade: %v\n", ph.poloniexChannel, row)
					continue
				}
				ph.tradeCh <- trade
			default:
				ph.metric.ErrorInc(ph.poloniexChannel, "error_unknown_row_type", rowType)
				ph.log.Printf("[%v] Unknown row type: %v\n", ph.poloniexChannel, row)
				continue
			}
		}
	}
}

func (ph *PoloniexHandler) stopHandler() {
	close(ph.stopCh)
}

func (ph *PoloniexHandler) stopHandlerListener() {
	go func() {
		for {
			select {
			case <-ph.stopCh:
				ph.log.Printf("[%v] Stop handler by internal command \n", ph.poloniexChannel)
				ph.Stop()
				return
			}
		}
	}()
}

//  Description: ["t", "<trade id>", <1 for buy 0 for sell>, "<price>", "<size>", <timestamp>, "<epoch_ms>"]
//  Example:     ["t", "42706057", 1, "0.05567134", "0.00181421", 1522877119, "1522877119341"]
func parseTrade(channel string, data []interface{}) (trade RecentTrade, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("error parse trade: %v", r)
		}
	}()

	if len(data) >= 7 {
		id := data[1].(string)
		tradeType := int(data[2].(float64))
		price := data[3].(string)
		size := data[4].(string)
		timestamp := data[5].(float64)

		parsedPrice, err := strconv.ParseFloat(price, 64)
		if err != nil {
			return trade, err
		}
		amount, err := strconv.ParseFloat(size, 64)
		if err != nil {
			return trade, err
		}
		var side string
		switch tradeType {
		case 0:
			side = "sell"
		case 1:
			side = "buy"
		default:
			return trade, fmt.Errorf("unknown trade type: %v", tradeType)
		}
		tradeTime := time.Unix(int64(timestamp), 0)
		return RecentTrade{
			ID:        id,
			Pair:      channel,
			Price:     parsedPrice,
			Amount:    amount,
			Side:      side,
			Timestamp: tradeTime,
		}, nil
	}
	return trade, fmt.Errorf("not enough len of data")
}
